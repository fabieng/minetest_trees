Trees
=====

Trees ? Trees ! Treeeeeeeeeeeeeeeeeeeeeeesss


Minetests custom-made trees.


Install
=======

Copy the content of the directory "schems" into 'worlds/<yourworld>/schems'.

Usage 
=====

Use the worldedit mod to load the trees into your world.

First set the position :

//pos1

Then load the tree

//load <name_tree>


Attention ! The trees are usually bigger than vanilla minetest trees. Load them in a clear area before moving them to the final destination.




License : WFTPL
